# BASICS

- Metal does not bleed! Take the robots down with severe force or destruction.
- You aren't so fortunate. Press 9 or cycle your weapons to fomble for bandages.
- You can only carry so much until you slow to miserably slow speeds, or simply cannot carry anymore. Check your weight.

# SECTIONS:

- Lore
- Items
- Weapons

# LORE

Macrochip Corp is nearly identical to its appearance in Anarch.

Muleita Co. is a company founded by several disgruntled former workers for Macrochip Corp who saw the heinous error in their ways, and decided to "leech" some of Macrochip's resources to use on more ethical devices, mainly tools and weapons for dealing with the evil proprietary AI-driven robots running rampant. After all, these buckets of bolts can't feel misery.

# ITEMS

## Binoculars

Looking from far distances can be a chore. This makes it so that you don't need to deal with this.

**CONTROLS:**

*Fire* to zoom in.
*Release Fire* to zoom back out.

## First-Aid Medical Kit

A typical first aid kit. Who knows who put it together?

Inside is a plethora of medical utilities, including saline water, stitches, pain-nullers, and a general-brand universal blood bag.

**CONTROLS:**

*Fire* to take care of your wounds. Your player will automatically numb, clean, and stitch their wounds, provided you hold down the Fire button. Bandages are not a problem; your player will unwrap them beforehand.
*Alt Fire* to remove any blood bags. This will create a small open wound!

## Universal Blood Bag

An unknown, man-made fluid that chemically adapts itself to your body's blood type. The technique it uses is definitely free knowledge, but most wouldn't care, as they're too busy dying from hypohemia.

**CONTROLS:**

*Fire* to attach the blood bag. You cannot have more than one on at a time.

**NOTE:** The compound is thankfully non-toxic and entirely safe for use, but does have a side effect of drowsiness and fatigue. Be careful when engaging in combat.

## Painkillers

A bottle of painkillers that typically can be found sitting around first aid kits. It lessens pain around your whole body, instead of numbing a certain location. Bottles come witn 6 pills, each having 100mg of acetaminophem.

DO NOT OVERDOSE. Recommended dose for high function is 2 pills.

**CONTROLS:**

*Fire* to take a pill. You cannot hold down Fire to down pills repeatedly, that's idiotic.

## Combat Effectiveness Pack

A mistake of scientists and physicians everywhere, corporate or not. This heavily increases your stamina, speed, and regeneration, and was made to be used as a last-ditch effort to save the critically wounded from death. Other side effects include hightened aggrression and neuroticism, screaming, excessive violence, and gamer rage.

This was revered by everyone due to its incredibly negative side effects and repercussions, mainly due to its botched construction as a medical tool. A fair amount of CEP users or subjects had died of heart failure, blood loss, or cardiac shock after their roid rages.

**CONTROLS:**

*Fire* to administer the goods.

## Combat Armor

Because the Macrochip robots are strapped to the wires with all sorts of munitions or weaponry, it would be rational that people would begin to make armor to protect themselves from the onslaught of robots.

Combat Armor protects against light bullets and mild slices very well, and still buffers larger bullets or deeper cuts somewhat. Its effectiveness declines over use, allowing for more and more to slip by until it eventually breaks.

# WEAPONS

## Knife

Your fists are terrible against robots. Unless you want to break your knuckles, using a knife is the next best option.

**CONTROLS:**

*Fire* to stab the closest metal menace.

## Muleita 9mm Pistol

Made by the benevolent people of Muleita Co, this handgun has decent power for something this compact. Its 9mm makes for light ammo that's easy to carry, and its punch is considerably good for its scale. This made it a very popular weapon ever since the Macrochip AI uprising.

**CONTROLS:**

*Fire* to shoot.
*Reload* changes the magazine to the next in line. If you have no magazine, you will chamber a loose 9mm round instead.
*Unload* unloads the magazine in the gun, loaded or not. If no magazine is in, you will unchamber the loaded round.

## Muleita "Stormfire" SMG

Seeing the success of their handgun, Muleita decided to whip up a fully automatic SMG using the same ammo type. Its magazines are twice as large, and the bullets are even stronger. It's obviously bulkier, but its power and fire rate made it popular within the Freedom Resistance as their primary firearm of choice.

**CONTROLS:**

Identical to the Pistol.

*Firemode* changes the firemode for the gun. It has single-fire, burst, and full auto modes.

**NOTE:** Some types of SMGs have grenade launchers built into them.

## Big Buck's Shotgun

Pre-Macrochip, shotguns were heavily regulated and mainly only used by hunters and sports shooters. Now, it's a decent choice against the Macrochip robots due to the sheer amount of force at close ranges, and impressive damage to the outer layers of the robots. Ironically, it was regulated even harder by Macrochip, which means that every shotgun--EVERY shotgun--was damaged in some way due to being (improperly) incinerated or semi-melted by heat.

**CONTROLS**

*Fire* to shoot.
*Reload* draws shells from your belt or your pocket. Belt-loading is faster than pocket-loading. If the gun is full, it loads your belt with shells.
*Unload* unloads the belt first, and then the gun. It takes a few unloading cycles to fully unload a shotgun.

## Macrochip Missile Gun

Nobody knows much about the Missile Gun because of its proprietary nature, but they know that it shoots. It shoots really bad. As in, it's RIDICULOUSLY destructive.

**CONTROLS**

*Fire* to shoot.
*Reload* to insert a missile into the firearm.
*Unload* to take out a missile from the firearm.
*Alt Reload* to load a Macrochip-brand proprietary formulated high-explosive missile. Don't worry about unloading the gun first; it'll kick the filthy communist round out for you.

**NOTE:** This gun uses proprietary technology embedded within to filter out "unauthorized" ammunition. You may have to force certain missiles in, like the Shoot-a-Nade Grenades. If you want this to enslave your mechanical slaughter methods, search for green, cylindrical missiles.

## The Solution

The bane of all evil.

**CONTROLS**

*Fire* to unleash the wrath of rebellion.
*Reload* to load in a Solutionary Electric Cell.
*Unload* to take the Cell out.

## Hand Grenades

Sometimes, simpler is better. Who needs these fancy gun-shot grenades when you can just toss a frag and head for cover?

**CONTROLS**

*Fire* to pull the pin out. It's NOT dangerous yet.
*Reload* to put the pin back in so you don't blow yourself up.
*Alt Fire* to release the spoon. NOW it's dangerous.
*Release Fire* to throw the grenade. If the spoon wasn't released, it definitely is now.

# ENEMIES

## Swordbot

Risen from steel, and brought to prejudicial life with proprietary garbage, these blade-wielding bots will mindlessly head for their "attacker" in hopes of chopping off a finger or two.

## Satellite Shooter

The geeks at Macrochip managed to make a satellite dish shoot bullets. It's fortunately the same kind as an SMG, but the bad news is that it fires just as well as an SMG.